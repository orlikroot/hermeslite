package ua.dp.krotov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.service.CategoryService;

/**
 * Created by Евгений on 07.07.2016.
 */
@RestController
@RequestMapping(value = "/api/category")
public class CategoryController
{
    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    ResponseEntity addCategory(@RequestParam("name") String name,
                               @RequestParam("parentId") long parentId) {

        Category category = new Category().builder()
                .name(name)
                .parent(categoryService.get(parentId))
                .build();
        categoryService.add(category);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity deleteCategory(@RequestBody Category category) {
        categoryService.delete(category.getId());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
//    @RequestMapping(value = "/breadcrumbs", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
//    @ResponseBody
//    public List<Category> getBreadCrumbsCategory(@RequestBody Category category) {
//
//        return categoryService.getBreadCrumbsCategory(category);
//    }

    @RequestMapping(value = "/get", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Category getBreadCrumbsCategory(@RequestBody Category category) {

        return categoryService.get(category.getId());
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity editCategory(@RequestBody Category category) {
        categoryService.edit(category);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
