package ua.dp.krotov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.dp.krotov.model.Image;
import ua.dp.krotov.model.Product;
import ua.dp.krotov.model.enums.ProductAvailability;
import ua.dp.krotov.model.enums.ProductCondition;
import ua.dp.krotov.service.CategoryService;
import ua.dp.krotov.service.ImageService;
import ua.dp.krotov.service.ProductService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Евгений on 13.07.2016.
 */
@RestController
@RequestMapping(value = "/api/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ImageService imageService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity addCategory(@RequestParam("name") String name ,
                               @RequestParam("description") String description,
                               @RequestParam("productCondition") String productCondition,
                               @RequestParam("productAvailability") String productAvailability,
                               @RequestParam("price") int price,
                               @RequestParam("categoryId") long categoryId,
                               @RequestParam(value = "image1", required = false) Long image1Id,
                               @RequestParam(value = "image2", required = false) Long image2Id,
                               @RequestParam(value = "image3", required = false) Long image3Id,
                               @RequestParam(value = "image4", required = false) Long image4Id,
                               @RequestParam(value = "image5", required = false) Long image5Id,
                               @RequestParam(value = "image6", required = false) Long image6Id,
                               @RequestParam(value = "image7", required = false) Long image7Id,
                               @RequestParam(value = "image8", required = false) Long image8Id,
                               @RequestParam(value = "image9", required = false) Long image9Id,
                               @RequestParam(value = "image10", required = false) Long image10Id


                               ) {
        List<Image> imageList = new ArrayList<>();

        Product product = new Product().builder()
                .name(name)
                .description(description)
                .productCondition(ProductCondition.valueOf(productCondition))
                .productAvailability(ProductAvailability.valueOf(productAvailability) )
                .price(price)
                .category(categoryService.get(categoryId))
                .images(imageList)
                .build();
        productService.add(product);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity deleteCategory(@RequestBody Product product) {
        productService.delete(product.getId());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Product getProduct(@RequestBody Product product) {

        return productService.get(product.getId());
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity editProduct(@RequestBody Product product) {
        productService.edit(product);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
