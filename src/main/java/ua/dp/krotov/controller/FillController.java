package ua.dp.krotov.controller;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.model.Image;
import ua.dp.krotov.model.Product;
import ua.dp.krotov.model.enums.ProductAvailability;
import ua.dp.krotov.model.enums.ProductCondition;
import ua.dp.krotov.model.enums.UserRole;
import ua.dp.krotov.model.enums.UserStatus;
import ua.dp.krotov.model.user.*;
import ua.dp.krotov.service.CategoryService;
import ua.dp.krotov.service.ImageService;
import ua.dp.krotov.service.ProductService;
import ua.dp.krotov.service.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Евгений on 09.06.2016.
 */
@Controller
@RequestMapping(value = "/api/test/fill")
public class FillController {

    @Autowired
    ImageService imageService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;


    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public
    @ResponseBody
    ResponseEntity persistImage() {
        for (int i = 0; i < 100; i++) {
            Image image = new Image().builder()
                    .url("http://www.localhost.dp.ua/image" + i + ".jpg")
                    .main(false)
                    .build();
            imageService.add(image);
        }

        for (int i = 0; i < 10; i++) {
            Category category = new Category()
                    .builder()
                    .name("Level 1 category " + i)
                    .build();
            categoryService.add(category);

            for (int y = 0; y < 10; y++) {

                Category subCategory = new Category()
                        .builder()
                        .name("Level 2 category " + y)
                        .parent(category)
                        .build();

                categoryService.add(subCategory);
                for (int z = 0; z < 10; z++) {

                    Category subSubCategory = new Category()
                            .builder()
                            .name("Level 3 category " + z)
                            .parent(subCategory)
                            .build();

                    categoryService.add(subSubCategory);
                }
            }
        }


        for (int i = 0; i < 100; i++) {
            List<Image> images = new ArrayList<>();
            images.add(new Image().builder().url("URL product image " + i * 1).build());
            images.add(new Image().builder().url("URL product image " + i * 10).build());
            images.add(new Image().builder().url("URL product image " + i * 100).build());

            Product product = new Product().builder()
                    .name("Test product № " + i)
                    .price(25)
                    .description("This is description of test product")
                    .productAvailability(ProductAvailability.IN_STOCK)
                    .productCondition(ProductCondition.NEW)
                    .category(categoryService.get(30L))
                    .images(images)
                    .build();

            productService.add(product);
        }

        for (int i = 0; i < 10; i++) {

            userService.add( new User().builder()
                    .securityData(new SecurityData().builder()
                            .email("pupkin" + i + "@i.ua")
                            .password("password")
                            .salt("ssssaaaaallllttttt")
                            .userRole(UserRole.USER_ROLE)
                            .build())
                    .userAddress(new UserAddress().builder()
                            .city("Dnipro")
                            .street("Lyashko Popelya")
                            .house("22/1")
                            .zip("49000")
                            .build())
                    .userDetails(new UserDetails().builder()
                            .firstName("Nikola")
                            .lastName("Tesla")
                            .phone("322-223")
                            .nickName("ElonMask" + i)
                            .about("I am mega man. That's all")
                            .userStatus(UserStatus.PRIVATE)
                            .build())
                    .history(new History().builder()
                            .createDate(new Date())
                            .updateDate(null)
                            .build()
                    ).build());

        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
}
