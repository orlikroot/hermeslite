package ua.dp.krotov.model;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Евгений on 08.06.2016.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Builder
public @Data  class Image implements Serializable {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;

    private String url;

    private Boolean main;
}
