package ua.dp.krotov.model.enums;

/**
 * Created by Евгений on 09.06.2016.
 */
public enum UserRole {
    USER_ROLE,
    ADMIN_ROLE
}
