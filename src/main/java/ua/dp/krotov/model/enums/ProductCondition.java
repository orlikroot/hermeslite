package ua.dp.krotov.model.enums;

import java.io.Serializable;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
public enum ProductCondition implements Serializable {
    NEW  ,
    USED
}
