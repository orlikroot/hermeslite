package ua.dp.krotov.model.enums;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
public enum Period {
   THREE(3), FIVE(5), SEVEN(7), FOURTEEN(14), THIRTY(30);

    private final int value;

    Period(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
