package ua.dp.krotov.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.dp.krotov.model.enums.Period;
import ua.dp.krotov.model.user.User;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Builder
public @Data class Lot {


    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;

    private Boolean isActive;

    private Date startDate;

    @Enumerated(EnumType.STRING)
    private Period period;

    private Date endDate;

    @OneToOne(fetch=FetchType.LAZY)
    private Product product;

    private int quantity;

    @OneToOne(fetch=FetchType.LAZY)
    private User seller;

    @OneToOne(fetch=FetchType.LAZY)
    private User buyer;

}
