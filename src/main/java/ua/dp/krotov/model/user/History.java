package ua.dp.krotov.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.dp.krotov.model.Lot;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Евгений on 09.06.2016.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public @Data class History {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;
    private Date createDate;
    private Date updateDate;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Lot> myPurchases;
}
