package ua.dp.krotov.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.dp.krotov.model.enums.UserStatus;


import javax.persistence.*;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public @Data class UserDetails {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;
    private String firstName;
    private String lastName;
    private @Enumerated(EnumType.STRING) UserStatus userStatus;
    @Column(unique = true)
    private String nickName;
    private String phone;
    private String about;
}
