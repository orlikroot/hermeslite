package ua.dp.krotov.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.dp.krotov.model.enums.UserRole;

import javax.persistence.*;

/**
 * Created by Евгений on 26.05.2016.
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public @Data class SecurityData {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;
    private @Column(unique = true) String email;
    private String password;
    private String salt;
    private UserRole userRole;

}
