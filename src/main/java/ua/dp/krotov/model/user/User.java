package ua.dp.krotov.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public @Data class User {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;

    @OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private SecurityData securityData;

    @OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private UserDetails userDetails;

    @OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private UserAddress userAddress;

    @OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    private History history;


}
