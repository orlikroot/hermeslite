package ua.dp.krotov.model.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by E.Krotov on 24.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public @Data class UserAddress {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO)  long id;
    private String city;
    private String zip;
    private String street;
    private String house;

}
