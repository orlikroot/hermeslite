package ua.dp.krotov.model;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.dp.krotov.model.enums.ProductAvailability;
import ua.dp.krotov.model.enums.ProductCondition;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by E.Krotov on 19.05.2016. (e.krotov@hotmail.com))
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Builder
public @Data class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    private String description;

    @Enumerated(EnumType.STRING)
    private ProductCondition productCondition;

    @Enumerated(EnumType.STRING)
    private ProductAvailability productAvailability;

    private int price;

    @ManyToOne(optional = false)
    private Category category;

    @OneToMany(cascade = CascadeType.ALL)
     private List<Image> images = new ArrayList<>(10);


}
