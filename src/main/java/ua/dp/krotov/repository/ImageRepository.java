package ua.dp.krotov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ua.dp.krotov.model.Image;

/**
 * Created by Евгений on 08.06.2016.
 */
@RepositoryRestResource(collectionResourceRel = "image", path = "image")
public interface ImageRepository extends JpaRepository<Image, Long> {
}
