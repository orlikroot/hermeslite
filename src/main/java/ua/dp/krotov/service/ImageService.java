package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.Image;
import ua.dp.krotov.repository.ImageRepository;

/**
 * Created by Евгений on 08.06.2016.
 */

public interface ImageService {

    void add(Image image);
    void delete(long id);
    Image getImage(long id);
}
