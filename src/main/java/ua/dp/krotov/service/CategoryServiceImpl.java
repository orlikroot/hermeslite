package ua.dp.krotov.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.Category;
import ua.dp.krotov.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Евгений on 09.06.2016.
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public void add(Category category) {
        categoryRepository.saveAndFlush(category);}

    @Override
    public void delete(long id) {
        categoryRepository.delete(id);}

    @Override
    public Category get(long id) {return categoryRepository.getOne(id);}

    // Edit can change only Name of category
    @Override
    public void edit(Category category) {
        Category categoryEdited = categoryRepository.getOne(category.getId());
        categoryEdited.setName(category.getName());
        categoryRepository.saveAndFlush(categoryEdited);
    }

//    @Override
//    public List<Category> getBreadCrumbsCategory(Category category) {
//        List<Category> categories = new ArrayList<>();
//        do  {categories.add(get(category.getId()));
//            category = get(category.getId()).getParent();}
//        while(category.getParent() != null);
//        categories.add(category);
//        return categories;
//    }

}
