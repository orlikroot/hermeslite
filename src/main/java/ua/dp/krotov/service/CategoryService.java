package ua.dp.krotov.service;

import ua.dp.krotov.model.Category;

import java.util.List;

/**
 * Created by Евгений on 09.06.2016.
 */
public interface CategoryService {
    void add(Category category);
    void delete(long id);
    Category get(long id);
//    List<Category> getBreadCrumbsCategory(Category category);
    void edit (Category category);

    }
