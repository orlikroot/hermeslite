package ua.dp.krotov.service;

import ua.dp.krotov.model.user.User;

import javax.jws.soap.SOAPBinding;

/**
 * Created by Евгений on 09.06.2016.
 */
public interface UserService {
    void add(User user);
    void delete(long id);
    User get(long id);
    void edit(User user);
}
