package ua.dp.krotov.service;

import ua.dp.krotov.model.Product;

/**
 * Created by Евгений on 09.06.2016.
 */
public interface ProductService {
    void add(Product product);
    void delete(long id);
    Product get(long id);
    void edit(Product product);
}
