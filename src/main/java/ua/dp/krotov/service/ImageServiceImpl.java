package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.Image;
import ua.dp.krotov.repository.ImageRepository;

/**
 * Created by Евгений on 08.06.2016.
 */
@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageRepository imageRepository;

    @Override
    public void add(Image image) {
        imageRepository.saveAndFlush(image);
    }

    @Override
    public void delete(long id) {
        imageRepository.delete(id);
    }

    @Override
    public Image getImage(long id) {
        return imageRepository.getOne(id);
    }
}
