package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.user.User;
import ua.dp.krotov.repository.UserRepository;

/**
 * Created by Евгений on 09.06.2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void add(User user) {
        userRepository.saveAndFlush(user);
    }

    @Override
    public void delete(long id) {
        userRepository.delete(id);
    }

    @Override
    public User get(long id) {
        return userRepository.getOne(id);
    }

    //TODO Implement method
    @Override
    public void edit(User user) {
        User user1 = userRepository.findOne(user.getId());
        user = user1;
        userRepository.saveAndFlush(user);
    }
}
