package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.model.Product;
import ua.dp.krotov.repository.ProductRepository;

/**
 * Created by Евгений on 09.06.2016.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public void add(Product product) {
        productRepository.saveAndFlush(product);
    }

    @Override
    public void delete(long id) {
        productRepository.delete(id);
    }

    @Override
    public Product get(long id) {
        return productRepository.getOne(id);
    }


    //TODO Check method
    @Override
    public void edit(Product product) {
        Product productEdited = productRepository.getOne(product.getId());
        productEdited.setName(product.getName());
        productEdited.setDescription(product.getDescription());
        productEdited.setPrice(product.getPrice());
        productEdited.setProductCondition(product.getProductCondition());
        productEdited.setProductAvailability(product.getProductAvailability());
        productEdited.setCategory(product.getCategory());
        productEdited.setImages(product.getImages());
        productRepository.saveAndFlush(productEdited);
    }
}
